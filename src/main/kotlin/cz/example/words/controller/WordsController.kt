package cz.example.words.controller

import cz.example.words.service.dto.WordDTO
import cz.example.words.service.WordsService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/words")
class WordsController(val wordsService: WordsService) {

    @GetMapping
    fun getWords() = wordsService.getWords().map { it.key }.toList()

    @GetMapping("{word}")
    fun getWord(@PathVariable word: String): WordDTO = wordsService.getWord(word)

    @PutMapping("{word}")
    fun updateWord(@PathVariable word: String, @RequestBody wordCategory: WordDTO.WordCategory): WordDTO =
            wordsService.putWord(word = WordDTO(word, wordCategory))
}