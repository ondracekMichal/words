package cz.example.words.controller

import cz.example.words.service.SentencesService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/sentences")
class SentencesController(val sentencesService: SentencesService) {

    @GetMapping
    fun getSentences(): List<String> = sentencesService.getAllSentences()

    @GetMapping("{sentenceId}")
    fun getSentence(@PathVariable sentenceId: Long): String = sentencesService.getSentence(sentenceId)

    @GetMapping("{sentenceId}/yodaTalk")
    fun getSentenceYodaTalk(@PathVariable sentenceId: Long): String = sentencesService.getSentenceYodaTalk(sentenceId)

    //TODO Should we generate unique sentences?
    @PostMapping("generate")
    fun generateSentence(): String = sentencesService.generateSentence()
}