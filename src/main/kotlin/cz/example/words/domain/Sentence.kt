package cz.example.words.domain

import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Sentence(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val sentenceId: Long = 0,
        val verb: String,
        val noun: String,
        val adjective: String,
        val generatedDate: LocalDateTime,
        val numberOfViews: Int = 0)