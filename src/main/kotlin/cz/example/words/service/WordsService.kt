package cz.example.words.service

import cz.example.words.service.dto.WordDTO
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException

@Service
class WordsService {

    private val words = mutableMapOf<String, WordDTO.WordCategory>()

    fun getWords(): MutableMap<String, WordDTO.WordCategory> = words

    fun getWord(word: String): WordDTO {
        val wordCategory = words[word] ?: throw IllegalArgumentException("Word $word doe not exist")
        return WordDTO(word, wordCategory)
    }

    fun putWord(word: WordDTO): WordDTO {
        words[word.word] = word.category
        return word
    }
}