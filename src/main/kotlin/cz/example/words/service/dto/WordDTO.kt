package cz.example.words.service.dto

data class WordDTO (
        val word: String,
        val category: WordCategory
) {
    enum class WordCategory { NOUN, VERB, ADJECTIVE }
}