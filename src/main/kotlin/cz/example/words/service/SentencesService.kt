package cz.example.words.service

import cz.example.words.domain.Sentence
import cz.example.words.repository.SentenceRepository
import cz.example.words.service.dto.WordDTO
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException
import java.time.LocalDateTime

@Service
class SentencesService(val sentenceRepository: SentenceRepository, val wordsService: WordsService) {

    fun generateSentence(): String {
        val noun = getRandomWord(WordDTO.WordCategory.NOUN)
        val verb = getRandomWord(WordDTO.WordCategory.VERB)
        val adjective = getRandomWord(WordDTO.WordCategory.ADJECTIVE)

        val sentence = Sentence(verb = verb, noun = noun, adjective = adjective, generatedDate = LocalDateTime.now())
        sentenceRepository.save(sentence)
        return "$noun $verb $adjective"
    }

    fun getAllSentences(): List<String> = sentenceRepository.findAll()
            .map { sentence -> "${sentence.noun} ${sentence.verb} ${sentence.adjective}" }

    @Synchronized
    fun getSentence(sentenceId: Long): String {
        val sentence = sentenceRepository.getOne(sentenceId)
        sentenceRepository.save(sentence.copy(numberOfViews = sentence.numberOfViews.inc()))
        return "${sentence.noun} ${sentence.verb} ${sentence.adjective}"
    }

    @Synchronized
    fun getSentenceYodaTalk(sentenceId: Long): String {
        val sentence = sentenceRepository.getOne(sentenceId)
        sentenceRepository.save(sentence.copy(numberOfViews = sentence.numberOfViews.inc()))
        return "${sentence.adjective} ${sentence.noun} ${sentence.verb}"
    }

    private fun getRandomWord(wordCategory: WordDTO.WordCategory): String  =
            wordsService.getWords()
                    .filter { it.value == wordCategory}
                    .toList()
                    .ifEmpty { throw IllegalArgumentException("$wordCategory are missing") }
                    .random().first

}