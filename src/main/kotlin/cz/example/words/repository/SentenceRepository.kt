package cz.example.words.repository

import cz.example.words.domain.Sentence
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SentenceRepository: JpaRepository<Sentence, Long>